package com.project;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public Login() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		DatabaseHandler dbHandler = new DatabaseHandler();
		dbHandler.openDB();
		String passRetrieved = dbHandler.getPassword(username);
		int userID = dbHandler.getUserID(username);
		System.out.println("UserID from DB"+userID);
		HttpSession session = request.getSession();
		session.setAttribute("userIDSession", userID);
		LoginResult result;

		if (password.equals(passRetrieved)) {
			result = new LoginResult(1, "Success");
			
		} else {
			result = new LoginResult(0, "Incorrect UserName or Password");
		}

		Gson gson = new Gson();
		String loginResult = gson.toJson(result);
		System.out.println(loginResult);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(loginResult);
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
