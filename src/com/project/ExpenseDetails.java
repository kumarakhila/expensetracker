package com.project;

import java.util.Date;

public class ExpenseDetails {
	
	int expenseID;
	int expense;
	String Description;
	String Category;
	int userID;
	Date dateAdded;
	
	private ExpenseDetails(int expenseID,int expense,String Description, String Category, int userID,Date dateAdded){
		this.expenseID = expenseID;
		this.expense = expense;
		this.Description = Description;
		this.Category=Category;
		this.userID=userID;
		this.dateAdded=dateAdded;
	}

	public void setExpenseID(int expenseID) {
		this.expenseID = expenseID;
	}

	public void setExpense(int expense) {
		this.expense = expense;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public int getExpenseID() {
		return expenseID;
	}

	public int getExpense() {
		return expense;
	}

	public String getDescription() {
		return Description;
	}

	public String getCategory() {
		return Category;
	}

	public int getUserID() {
		return userID;
	}

	public Date getDateAdded() {
		return dateAdded;
	}
	
	
	
	
}
