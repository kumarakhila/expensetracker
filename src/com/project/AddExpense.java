package com.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddExpense
 */
@WebServlet("/AddExpense")
public class AddExpense extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddExpense() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int getExpense = Integer.parseInt(request.getParameter("expense"));
		
		String getDesc = request.getParameter("desc");
		String getCategory = request.getParameter("category");
		String getDateString = request.getParameter("dateAdded");
		SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
		
		Date getDate=null;
		
		try {
			getDate = sdf.parse(getDateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		DatabaseHandler dbao = new DatabaseHandler();
		dbao.openDB();
		LoginResult output;
		
		
		HttpSession session = request.getSession(false);
		int userID = (int) session.getAttribute("userIDSession");
		
		
		boolean result = dbao.addExpense(getExpense, getDesc, getCategory,userID,getDate);
		
		if(result){
			output = new LoginResult(200, "OK");
			
		}else{
			output = new LoginResult(500, "DB Not updated");
		}
		Gson gson = new Gson();
		String addExpenseRes = gson.toJson(output);
		System.out.println(addExpenseRes);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(addExpenseRes);
		out.flush();
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
