package com.project;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


public class DatabaseHandler {
	  private java.sql.Connection connect = null;
	  private PreparedStatement verifyUserPreparedStatement = null;
	  private PreparedStatement addExpensePS = null;
	  private PreparedStatement getUserIDPS = null;
	  private PreparedStatement getUserNamePS = null;
	  private static final String dbName = "LOGININFO";
	  private static final String tableAddExpense = "AddExpense";
	  private static final String retrivePassword= "SELECT PASSWORD FROM " + dbName+ "  WHERE USERNAME = ?";
	  private static final String addExpense = "INSERT INTO " + tableAddExpense + " (expense,description,category,userID,dateAdded) values(?,?,?,?,?)";
	  private static final String userID = "SELECT IDLOGININFO FROM " +dbName+ " WHERE USERNAME = ?";
	  private static final String userName = "SELECT USERNAME FROM "+dbName+ " WHERE IDLOGININFO = ? ";
	
	void openDB(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
//			connect = DriverManager
//			          .getConnection("jdbc:mysql://localhost:3306/NewProject?"
//			                  + "user=root&password=root");
			
			
			connect = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/NewProject", "root",
                    "root");
			
			
			verifyUserPreparedStatement = connect.prepareStatement(retrivePassword);
			addExpensePS = connect.prepareStatement(addExpense);
			getUserIDPS=connect.prepareStatement(userID);
			getUserNamePS= connect.prepareStatement(userName);
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		 
		 
	}
	
	 String getPassword(String username){
		 String passwordFromDB=null;
		try {
			
			verifyUserPreparedStatement.setString(1, username);
			ResultSet rs = verifyUserPreparedStatement.executeQuery();
			
			
			if(rs.next()){
				 passwordFromDB = rs.getString("password");
				
			}else{
				passwordFromDB = null;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	return 	passwordFromDB;
	}
	 
	 int getUserID(String username){
		 int userID=0;
			try {
				getUserIDPS.setString(1, username);
				ResultSet rs = getUserIDPS.executeQuery();
				
				
				if(rs.next()){
					 userID = rs.getInt("IDLOGININFO");
					
				}else{
					userID = -1;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 return userID;
	 }
	 
	 String getUserName(int userID){
		 String userNameFromDB=null;
			try {
				
				getUserNamePS.setInt(1, userID);
				ResultSet rs = getUserNamePS.executeQuery();
				
				
				if(rs.next()){
					userNameFromDB = rs.getString("username");
					
				}else{
					userNameFromDB = null;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		return 	userNameFromDB;
	 }
	 boolean addExpense(int expense, String desc, String category, int userID, Date dateAdded){
		 boolean result;
		 try {
			 
			addExpensePS.setInt(1, expense);
			addExpensePS.setString(2, desc);
			addExpensePS.setString(3, category);
			addExpensePS.setInt(4, userID);
			java.sql.Date dateToInsert = new java.sql.Date(dateAdded.getTime());
			addExpensePS.setDate(5, dateToInsert);
			result = addExpensePS.executeUpdate() == 1 ? true:false;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		}
		 
		 return result;
	 }
	 
}

